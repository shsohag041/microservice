﻿using Play.Catalog.Service.Entities;
using MongoDB.Driver;
using System.Threading.Tasks;
using System.Collections.Generic;
using System;
using System.Reflection.Metadata;

namespace Play.Catalog.Service.Repositories
{
    
    public class ItemsRepository : IItemsRepository
    {
        private const string collectionName = "items";
        private readonly IMongoCollection<Item> dbCollection;
        private readonly FilterDefinitionBuilder<Item> filterBuilder = Builders<Item>.Filter;//all item collection

        public ItemsRepository(IMongoDatabase database)
        {
          //  var mongoClient = new MongoClient("mongodb://localhost:27017");
           // var database = mongoClient.GetDatabase("Cataloge");
            dbCollection = database.GetCollection<Item>(collectionName);
        }

        public async Task<IReadOnlyCollection<Item>> GetItemsAsync()
        {
            return await dbCollection.Find(filterBuilder.Empty).ToListAsync();
        }

        public async Task<Item> GetAsync(Guid id)
        {
            FilterDefinition<Item> filter = filterBuilder.Eq(Entity => Entity.Id, id);
            return await  dbCollection.Find(filter).FirstOrDefaultAsync();
        }
        public async Task CreateAsync(Item entity)
        {
            if (entity == null) {
                throw new ArgumentNullException(nameof(entity));
            }
           await dbCollection.InsertOneAsync(entity);
        }

        public async Task UpdateAsync(Item entity)
        {
            if(entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            FilterDefinition<Item> filter = filterBuilder.Eq(existEntity => existEntity.Id, entity.Id);
             await dbCollection.ReplaceOneAsync(filter, entity);

        }
        public async Task RemoveAsync(Guid id)
        {
            FilterDefinition<Item> filter = filterBuilder.Eq(entity => entity.Id, id);
            await dbCollection.DeleteOneAsync(filter);
        }
    }
}

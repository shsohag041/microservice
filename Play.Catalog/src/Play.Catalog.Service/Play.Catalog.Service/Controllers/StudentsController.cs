﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Play.Catalog.Service.CollectionDtos;
using Play.Catalog.Service.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using static Play.Catalog.Service.CollectionDtos.StudentDtos;

namespace Play.Catalog.Service.Controllers
{
    [Route("Students/[controller]")]
    [ApiController]
    public class StudentsController : ControllerBase
    {
        private static readonly List<StudentDto> students = new List<StudentDto>
        {
            new StudentDto(Guid.NewGuid(),"Hasan","seven"),
             new StudentDto(Guid.NewGuid(),"Abu","eight"),
              new StudentDto(Guid.NewGuid(),"Sayem","seven"),
              new StudentDto(Guid.NewGuid(),"Ahmmed","nine"),
              new StudentDto(Guid.NewGuid(),"Shariful","seven"),
              new StudentDto(Guid.NewGuid(),"Sohag","eight"),
              new StudentDto(Guid.NewGuid(),"Lima","seven"),
        };
        [HttpGet]
        
        public IEnumerable<StudentDto> getAll()
        {
            return students;
        }

        [HttpGet("id")]
        public StudentDto GetById(Guid id)
        {
            var student = students.Where(student => student.Id == id).FirstOrDefault();
            return student;
        }
        [HttpPost]
        public ActionResult<StudentDto> Post(CreateStudentDto createStudentDto)
        {
            var student = new StudentDto(Guid.NewGuid(),createStudentDto.Name,createStudentDto.ClassRoom);
            students.Add(student);
            return student;
        }
        [HttpPut("id")]

        public IActionResult Put(Guid id, UpdateItemDto
        updateItemDto)
        {
            var existingStudent = students.Where(x => x.Id == id).FirstOrDefault();
            var updateItem = existingStudent with
            {
                Name = updateItemDto.Name,
                ClassRoom = updateItemDto.Description,
            };
            var index = students.FindIndex(existingItem => existingItem.Id == id);
            students[index] = updateItem;
            return NotFound();
        }
        [HttpDelete("id")]
        public IActionResult Delete(Guid id)
        {
            var index = students.FindIndex(existingStudent => existingStudent.Id == id);
            students.RemoveAt(index);

            return NotFound();
        }
    }
}

﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Play.Catalog.Service.Dtos;
using Play.Catalog.Service.Entities;
using Play.Catalog.Service.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Play.Catalog.Service.Controllers
{
    [Route("Items/[controller]")]
    [ApiController]
    public class ItemsController : ControllerBase
    {
        public readonly IItemsRepository itemsRepository;

        public ItemsController(IItemsRepository itemsRepository)
        {
            this.itemsRepository = itemsRepository;
        }

        [HttpGet]
        public async Task<IEnumerable<ItemDto>> GetItemsAsync()
        {
            var items = (await itemsRepository.GetItemsAsync()).Select(item => item.AsDto());
            return items;
        }
        [HttpGet("id")]
        public async Task< ActionResult<ItemDto>> GetByIdAsync(Guid id)
        {
            var item = await itemsRepository.GetAsync(id);
            if (item == null)
            {
                return NotFound();
            }
           
            return item.AsDto();
        }
        [HttpPost] 
        public async Task< ActionResult<ItemDto>> PostAsync (CreateItemDto createItemDto)
        {
            var item = new Item()
            {
                //Id = Guid.NewGuid(),
                Name = createItemDto.Name,
                Description = createItemDto.Description,
                Title = createItemDto.Title,
                Price = createItemDto.Price
            };
            await itemsRepository.CreateAsync(item);
            return CreatedAtAction(nameof(GetByIdAsync),new {id=item.Id},item  );
            //return (item.AsDto());
        }
        [HttpPut("id")]

       public async Task<ActionResult> PutAsync(Guid id,UpdateItemDto
            updateItemDto)
        {
            var item = await itemsRepository.GetAsync(id);
            if (item == null)
            {
                return NotFound();
            }

            item.Name = updateItemDto.Name;
            item.Description = updateItemDto.Description;
            item.Title = updateItemDto.Title;
            item.Price = updateItemDto.Price;
            await itemsRepository.UpdateAsync(item);
           
            return NotFound();
        }
        [HttpDelete("id")]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {

            var item = await itemsRepository.GetAsync(id);
            if (item == null)
            {
                return NotFound();
            }
            await itemsRepository.RemoveAsync(item.Id);

            return NotFound();
        }
    }
}

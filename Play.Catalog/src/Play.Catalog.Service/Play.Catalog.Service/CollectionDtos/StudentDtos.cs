﻿using System;

namespace Play.Catalog.Service.CollectionDtos
{
    public class StudentDtos
    {
        public record StudentDto(Guid Id, string Name, string ClassRoom);
        public record CreateStudentDto(string Name, string ClassRoom);
        public record UpdateStudentDto(string Name, string ClassRoom);


    }
}

﻿using System;

namespace Play.Catalog.Service.Dtos
{
    public record ItemDto(Guid Id, string Name, string Description, string Title, decimal Price);
    public record CreateItemDto( string Name, string Description, string Title, decimal Price);
    public record UpdateItemDto(string Name, string Description, string Title, decimal Price);

}

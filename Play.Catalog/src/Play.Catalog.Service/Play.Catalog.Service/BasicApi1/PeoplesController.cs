﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using static Play.Catalog.Service.BasicApi1.PeopleDtos;
using System.Linq;

namespace Play.Catalog.Service.BasicApi1
{
    [Route("api1/[controller]")]
    [ApiController]
    public class PeoplesController : ControllerBase
    {
        private static readonly List<PeopleDto> peoples = new List<PeopleDto>
        {
             new  PeopleDto(Guid.NewGuid(),"Hasan","Room number seven"),
             new  PeopleDto(Guid.NewGuid(),"Abu","Room number eight"),
              new PeopleDto(Guid.NewGuid(),"Sayem","Room number seven"),
              new PeopleDto(Guid.NewGuid(),"Ahmmed","Room number nine"),
              new PeopleDto(Guid.NewGuid(),"Shariful","Room number seven"),
              new PeopleDto(Guid.NewGuid(),"Sohag","Room number eight"),
              new PeopleDto(Guid.NewGuid(),"Lima","Room number seven"),
        };

        [HttpGet]

        public IEnumerable<PeopleDto> getAll()
        {
            return peoples;
        }
        [HttpGet("id")]
        public PeopleDto GetById(Guid id)
        {
            var student = peoples.Where(student => student.Id == id).FirstOrDefault();
            return student;
        }
        [HttpPost]
        public ActionResult<PeopleDto> Post(CreatePeopleDto createPeopleDto)
        {
            var student = new PeopleDto(Guid.NewGuid(), createPeopleDto.Name, createPeopleDto.Room);
            peoples.Add(student);
            return student;
        }
        [HttpPut("id")]

        public IActionResult Put(Guid id, UpdatePeopleDto
        updatePeopleDto)
        {
            var existingStudent = peoples.Where(x => x.Id == id).FirstOrDefault();
            var updateItem = existingStudent with
            {
                Name = updatePeopleDto.Name,
                Room = updatePeopleDto.Room,
            };
            var index = peoples.FindIndex(existingItem => existingItem.Id == id);
            peoples[index] = updateItem;
            return NotFound();
        }
        [HttpDelete("id")]
        public IActionResult Delete(Guid id)
        {
            var index = peoples.FindIndex(existingStudent => existingStudent.Id == id);
            peoples.RemoveAt(index);

            return NotFound();
        }

    }
}

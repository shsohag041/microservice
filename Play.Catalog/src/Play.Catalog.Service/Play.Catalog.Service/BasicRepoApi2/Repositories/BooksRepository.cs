﻿using MongoDB.Driver;
using Play.Catalog.Service.BasicRepoApi2.Entities;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Play.Catalog.Service.BasicRepoApi2.Repositories
{
    public class BooksRepository

    {
        private const string collectionName = "books";
        private readonly IMongoCollection<Book> dbCollection;
        private readonly FilterDefinitionBuilder<Book> filterBuilder = Builders<Book>.Filter;//all item collection

        public BooksRepository()
        {
            var mongoClient = new MongoClient("mongodb://localhost:27017");
            var database = mongoClient.GetDatabase("Catalog");
            dbCollection = database.GetCollection<Book>(collectionName);
        }
        public async Task<IReadOnlyCollection<Book>> GetBooksAsync()
        {
            var list = await dbCollection.Find(filterBuilder.Empty).ToListAsync();
            return list;
        }

        public async Task<Book> GetAsync(Guid id)
        {
            FilterDefinition<Book> filter = filterBuilder.Eq(Entity => Entity.Id, id);
            return await dbCollection.Find(filter).FirstOrDefaultAsync();
        }
        public async Task CreateAsync(Book entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            await dbCollection.InsertOneAsync(entity);
        }

        public async Task UpdateAsync(Book entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            FilterDefinition<Book> filter = filterBuilder.Eq(existEntity => existEntity.Id, entity.Id);
            await dbCollection.ReplaceOneAsync(filter, entity);
        }
        public async Task RemoveAsync(Guid id)
        {
            FilterDefinition<Book> filter = filterBuilder.Eq(entity => entity.Id, id);
            await dbCollection.DeleteOneAsync(filter);
        }

    }
   

}

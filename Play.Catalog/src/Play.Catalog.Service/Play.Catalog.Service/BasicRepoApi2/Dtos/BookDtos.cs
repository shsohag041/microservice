﻿using System;

namespace Play.Catalog.Service.BasicRepoApi2.Dtos
{
    public class BookDtos
    {
        public record BookDto(Guid Id, int Uid,  string Name, string Description, string Author, int Student_uid, int Teacher_uid);
        public record CreateBookDto(int Uid, string Name, string Description, string Author, int Student_uid, int Teacher_uid);
        public record UpdateBookDto(int Uid, string Name, string Description, string Author, int Student_uid, int Teacher_uid);
    }
}

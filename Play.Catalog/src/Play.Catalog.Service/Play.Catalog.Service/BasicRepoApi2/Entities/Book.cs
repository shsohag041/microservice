﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Play.Catalog.Service.BasicRepoApi2.Entities
{
    public class Book
    {
        [Required]
        public Guid Id { get; set; }
        public int Uid { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Author { get; set; }
        public int Student_uid {get;set;}
        public int Teacher_uid { get; set; }
    }
}

﻿using System;

namespace Play.Catalog.Service.BasicRepoApi2.Entities
{
    public class Teacher
    {
        public Guid Id { get; set; }
        public int uid { get; set; }
        public string Name { get; set; }
       
    }
}

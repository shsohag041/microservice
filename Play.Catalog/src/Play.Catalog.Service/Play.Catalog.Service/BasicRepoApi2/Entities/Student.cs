﻿using System.ComponentModel.DataAnnotations;
using System;

namespace Play.Catalog.Service.BasicRepoApi2.Entities
{
    public class Student
    {
        [Required]
        public Guid Id { get; set; }
        public int uid { get; set; }
        public int Book_uid { get; set; }
        public string Name { get; set; }
        public int teacher_uid { get; set; }
    }
}

﻿using Play.Catalog.Service.BasicRepoApi2.Entities;
using static Play.Catalog.Service.BasicRepoApi2.Dtos.BookDtos;

namespace Play.Catalog.Service.BasicRepoApi2.Mapping
{
    public static class BookEntityDtoMapping
    {
        public static BookDto AsDto(this Book book)
        {
            return new BookDto(book.Id,book.Uid, book.Name, book.Description, book.Author, book.Student_uid,book.Teacher_uid);

        }

    }
    
}
